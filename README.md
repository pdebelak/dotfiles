# Env configuration

This is my custom shell configuration.

## Instructions

### Prerequisites

First, ensure you have ssh keys set up since the install script
assumes you can clone git repos using ssh.

Then, see the [packages documentation](./packages/) for a list of
packages needed.

### Installation

Run `curl https://gitlab.com/pdebelak/dotfiles/-/raw/master/install.sh
| sh` to install.

If you're not brave enough to pipe curl into shell, then do:

```shell
curl https://gitlab.com/pdebelak/dotfiles/-/raw/master/install.sh > install.sh
sh install.sh
rm install.sh
```

### Postrequisites

See the [packages documentation](./packages/) for other packages to
install.
