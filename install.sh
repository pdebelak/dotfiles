#!/bin/sh
# clones the dotfiles and ptd-project repos and symlinks them to expected locations

set -e

projects_dir="$HOME/Projects"

mkdir -p "$projects_dir"

if ! command -v git >/dev/null; then
  echo 'This script uses git, please install that first' >&2
  exit 1
fi

# set up dotfiles
dotfiles_dir="$projects_dir/dotfiles"
if ! [ -d "$dotfiles_dir" ]; then
  git clone git@gitlab.com:pdebelak/dotfiles.git "$dotfiles_dir"
fi

export PATH="$HOME/.local/bin:$PATH"

cd "$dotfiles_dir"
./symlink.sh
./install_oils.sh || \
  echo 'Failed to install oil, you likely need a c compiler and libreadline dev headers' >&2
./python_info.sh || \
  echo 'Failed to install python docs' >&2
./install_python_bins.sh || \
  echo 'Failed to install python programs' >&2
./sqlfluff_info.sh || \
  echo 'Failed to install sqlfluff docs' >&2
./install_go_bins.sh || \
  echo 'Failed to install go programs' >&2
if ./install_quickjs.sh; then
  # compile and install ptd-jq
  cd "$dotfiles_dir/programs/ptd-jq"
  make
  make install
else
  echo 'Failed to install quickjs' >&2
fi
./install_jdtls.sh  || \
  echo 'Failed to install jdtls' >&2

cd "$projects_dir"

machine_specific_file="$HOME/.emacs.d/custom/machine-specific.el"
if ! [ -f "$machine_specific_file" ]; then
  cat "$dotfiles_dir/emacs.d/machine-specific-template.el" > "$machine_specific_file"
fi

ptd_project_dir="$projects_dir/ptd-project"
if ! [ -d "$ptd_project_dir" ]; then
  git clone git@gitlab.com:pdebelak/ptd-project.git "$ptd_project_dir"
fi
ln -sf "$ptd_project_dir/ptd-project.el" "$HOME/.emacs.d/custom/ptd-project.el"

if ! command -v emacs >/dev/null; then
  echo "Don't forget to install emacs"
fi
