ptd_searchable_dir_names() {
  for dir in "$HOME/Projects" "$HOME" "$HOME/Music" "$HOME/Movies"; do
    if [ -d "$dir" ]; then
      for f in "$dir"/*; do
        if [ -d "$f" ]; then
          echo "$f"
        fi
      done
    fi
  done
}

ptd_z() {
  target="$1"

  if [ -z "$target" ]; then
    return 1
  fi

  target_dir="$(ptd_searchable_dir_names | grep -Ei "/${target}[^/]*\$" | head -1)"

  if [ -z "$target_dir" ]; then
    target_dir="$(ptd_searchable_dir_names | grep -Ei "/[^/]*${target}[^/]*\$" | head -1)"
  fi

  if [ -z "$target_dir" ]; then
    return 1
  fi

  cd "$target_dir" || return 1
}

ptd_open() {
  if command -v xdg-open >/dev/null; then
    # most linuxes
    xdg-open "$@"
  elif command -v explorer.exe >/dev/null; then
    # wsl
    explorer.exe "$@"
  else
    command open "$@"
  fi
}

ptd_edit() {
  eval $EDITOR "$@"
}

ptd_python_version() {
  "$1" --version | awk '{ print $2 }'
}

ptd_create_venv() {
  python="${1:-python3}"
  venv="${2:-.venv}"
  "$python" -m venv "$venv"
  "$venv/bin/python" -m pip install --upgrade pip wheel setuptools
  . "$venv/bin/activate"
}

ptd_replace_venv() {
  if [ -n "$VIRTUAL_ENV" ]; then
    venv="$VIRTUAL_ENV"
    deactivate
  else
    venv="${1:-.venv}"
  fi
  if ! [ -d "$venv" ]; then
    echo "Couldn't find virtual env at $venv" >&2
    return 1
  fi
  python_version="$(ptd_python_version "$venv/bin/python3")"
  if [ -d "$HOME/.pyenv/versions/$python_version" ]; then
    python="$HOME/.pyenv/versions/$python_version/bin/python"
  elif [ "$(ptd_python_verison python3)" = "$python_version" ]; then
    python="python3"
  else
    echo "Couldn't find matching python for $python_version" >&2
    return 1
  fi
  rm -rf "$venv"
  ptd_create_venv "$python" "$venv"
}
