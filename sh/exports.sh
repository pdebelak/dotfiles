case "$PATH" in
  (*"$HOME/.local/bin"*)
    # already in path
    ;;
  (*)
    export PATH="$HOME/.local/bin:$PATH"
    ;;
esac

if [ -f "$HOME/.cargo/env" ]; then
  . "$HOME/.cargo/env"
fi

export GOPATH="$HOME/.local/share/go"
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000
export HISTFILESIZE=30000
export EDITOR="emacs -nw"
export VISUAL="$EDITOR"
export KEYTIMEOUT=1

if command -v brew >/dev/null; then
  eval "$(brew shellenv)"
elif [ -x /opt/homebrew/bin/brew ]; then
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# prompt setup
reset_color() {
  echo '\[\e[00m\]'
}

set_color() {
  color_num="$1"
  echo '\[\e['"${color_num}m\]"
}

parse_git_branch() {
  git branch --no-color 2>/dev/null | awk '$1 == "*" {
    "git status --short 2>/dev/null" | getline status
    if (status != "") {
      gitdirty = "*"
    }
    printf("‹%s%s› ", $2, gitdirty)
    exit
  }'
}

non_default_shell_name() {
  # expects to be called with $0 as an arg and prints the shell name
  # if it doesn't match $SHELL
  if test "$(basename "$SHELL")" = "$(basename "${1#-}")"; then
    : # we're using the default shell, do nothing
  else
    printf "(%s)\n" "$1"
  fi
}

prompt_sigil() {
  echo '→'
}

PS1="$(non_default_shell_name $0)$(set_color 32)\w$(reset_color) $(set_color 33)"'$(parse_git_branch)'"$(reset_color)$(prompt_sigil) "
export PS1
