# Shell Aliases

alias z='ptd_z'
alias open='ptd_open'
alias e='ptd_edit'

# Git Aliases
alias gs='git status'
alias gcm='git commit -m'
alias gco='git checkout'
alias ga='git add -A'
alias gl='git log'
alias gd='git diff'
alias gb='git branch'
alias gdc='git diff --cached -w'
alias gpl='git pull'
alias gplr='git pull --rebase'
alias gpsh='git push -u origin `git rev-parse --abbrev-ref HEAD`'
alias gnb='git checkout -b'
alias gam='git amend --reset-author'
