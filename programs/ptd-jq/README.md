ptd-jq
======

A [jq](https://stedolan.github.io/jq/)-type program that just uses
javascript via [quickjs](https://bellard.org/quickjs/quickjs.html).

## Explanation

[jq](https://stedolan.github.io/jq/) is great, but it's another
custom language to learn. As an experiment, this program just parses
each line of `stdin` as `JSON` and then passes that as `data` to one
or more javascript snippets.

## Example

```shell
$ curl https://api.reverb.com/api/listings 2>/dev/null | ptd-jq 'data.listings' 'data.map(l => `${l.make} ${l.model}`)'
[
  "Fender American Ultra Telecaster with Maple Fretboard",
  "Electric Majik Albion",
  "Agile 3010",
  "DiMarzio DP151FBK PAF Pro F-Spaced Humbucker",
  "Walrus Audio Mako R1 High-Fidelity Reverb",
  "PPG 360",
  "Gibson Les Paul Studio",
  "Slingerland vintage double tom stands",
  "Gibson 490R Modern Classic Neck Humbucker",
  "Sterling Stingray",
  "ThorpyFX The Dane Peter Honore Signature Overdrive and Boost",
  "DW DWCP5000AD4 5000 Series Accelerator Single Bass Drum Pedal",
  "Seymour Duncan SPH90-1n Phat Cat Neck Pickup",
  "Ibanez SRC6WNF Electric Bass",
  "Roland JC-40 Jazz Chorus 2-Channel 40-Watt 2x10\" Guitar Combo",
  "Fender American Standard Telecaster",
  "Premier Lokfast Snare Drum stand",
  "Gibson 57 - Zebra, Recent",
  "Wurlitzer 200",
  "Ibanez 2012 Ibanez S421 Electric Guitar! Wizard Neck! Blackberry Sunburst Finish! NICE!",
  "DigiTech Bass Squeeze",
  "Electro-Harmonix Holy Grail Reverb",
  "Native Instruments Komplete Kontrol A49",
  "TC Electronic Nova System"
]
```

## Installation

First, you need to install
[quickjs](https://bellard.org/quickjs/quickjs.html) since this uses
the `qjsc` compiler. Then, just run `make install` which will install
`ptd-jq` to `~/.local/bin`. Edit the `prefix` in the `Makefile` to
change this location.
