import * as std from 'std';

const progs = scriptArgs.splice(1);
progs.push('data');

let gatheredJSON = '';
while (!std.in.eof()) {
  const line = std.in.getline();
  if (line) {
    gatheredJSON += line;
    let parsedJSON;
    try {
      parsedJSON = JSON.parse(gatheredJSON);
    } catch (e) {
      if (e instanceof SyntaxError) {
        // This is the error we get for invalid json. Go to the next
        // line to see if it becomes valid.
        continue;
      } else {
        throw e;
      }
    }
    gatheredJSON = '';
    const data = progs.reduce((data, prog) => eval(prog), parsedJSON);
    if (typeof data !== 'undefined') {
      std.out.puts(JSON.stringify(data, null, 2) + '\n');
    }
  }
}

if (gatheredJSON.length > 0) {
  std.err.puts(`Failed to parse json: ${gatheredJSON}\n`);
  std.exit(1);
}
