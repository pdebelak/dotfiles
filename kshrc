if command -v fasd 1>/dev/null 2>&1; then
  # the sed makes removes the local declaration for ksh typeset var
  eval "$(fasd --init posix-alias posix-hook | sed 's/local/typeset var/g')"
fi

for config_file in "$HOME"/.sh/*.sh; do
  . "$config_file"
done

reset_color() {
  echo '[00m'
}

set_color() {
  color_num="$1"
  echo '['"${color_num}m"
}

# oksh has no history unless HISTFILE is specified
export HISTFILE="$HOME/.ksh_history"
export HISTCONTROL=ignoredups:ignorespace
export PS1="$(non_default_shell_name $0)$(set_color 32)"'$(if [ "${PWD#$HOME}" != "$PWD" ]; then printf "~%s" "${PWD#$HOME}"; else printf "%s" "$PWD"; fi)'"$(reset_color) $(set_color 33)"'$(parse_git_branch)'"$(reset_color)$(prompt_sigil) "