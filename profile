if [ -n "$ZSH_VERSION" ]; then
  . "$HOME/.zshrc"
elif [ -n "$BASH_VERSION" ]; then
  . "$HOME/.bashrc"
elif [ -n "$KSH_VERSION" ]; then
  . "$HOME/.kshrc"
fi
