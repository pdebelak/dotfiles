#!/bin/sh

set -e

sqlfluff_version="2.0.3"

check_bin() {
  bin_name="$1"
  if ! command -v "$@" >/dev/null; then
    echo "This script requires $bin_name, please install that first" >&2
    exit 1
  fi
}

check_bin install-info

curdir="$(pwd)"
workingdir="$curdir/sqlfluff-tmp"

# download the source code
rm -rf "$workingdir"
git clone https://github.com/sqlfluff/sqlfluff.git "$workingdir"
cd "$workingdir"
git checkout "$sqlfluff_version"

# install deps in virtual env
python3 -m venv .venv
.venv/bin/pip install --upgrade pip wheel setuptools
.venv/bin/pip install -e "$workingdir"
.venv/bin/pip install -r docs/requirements.txt
. .venv/bin/activate

# build and install the docs
make -C docs info
make -C docs/build/texinfo infodir="$HOME/.local/share/info" install-info

# clean up
cd "$curdir"
rm -rf "$workingdir"
