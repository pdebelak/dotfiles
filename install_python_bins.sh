#!/bin/sh

set -e

share_dir="$HOME/.local/share"
mkdir -p "$share_dir"
venv_dir="$share_dir/venv"
python3 -m venv "$venv_dir"
"$venv_dir/bin/pip" install --upgrade pip wheel setuptools
"$venv_dir/bin/pip" install -r requirements.txt -c constraints.txt

for bin in black black-macchiato flake8 isort mypy pylsp sqlfluff; do
  ln -sf "$venv_dir/bin/$bin" "$HOME/.local/bin/$bin"
done
