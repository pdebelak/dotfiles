#!/bin/sh
# Installs quickjs to ~/.local

set -e

QJS_REPO='https://github.com/bellard/quickjs.git'
BUILD_DIR="qjs-build-dir"
PREFIX="$HOME/.local"

mkdir -p "$BUILD_DIR"
git clone "$QJS_REPO" "$BUILD_DIR"
cd "$BUILD_DIR"
sed "s|prefix=/usr/local|prefix=$PREFIX|" Makefile > Makefile.local
mv Makefile.local Makefile
make
make install
cd ..
rm -rf "$BUILD_DIR"
