if command -v fasd 1>/dev/null 2>&1; then
  eval "$(fasd --init auto)"
fi

ASDF="$HOME/.asdf/asdf.sh"
if [ -f "$ASDF" ]; then
  . "$ASDF"
  . "$HOME/.asdf/completions/asdf.bash"
fi

# allow multiple bash sessions to not overwrite each other
shopt -s histappend

# patterns which fail to match filenames during filename expansion
# result in an expansion error
shopt -s failglob

for config_file in "$HOME"/.sh/*.sh; do
  . "$config_file"
done
