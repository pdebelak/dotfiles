;;; machine-specific.el --- Machine-specific setup code for emacs

;;; Commentary:

;; Should contain any code that is specific to a single machine.

;;; Code:

(provide 'machine-specific)
;;; machine-specific.el ends here
