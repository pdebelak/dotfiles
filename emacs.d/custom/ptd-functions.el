;;; ptd-functions.el --- Various elisp functions

;;; Commentary:

;; Some custom functions

;;; Code:

(require 'ptd-project)
(require 'gud)

(defvar ptd-insert-breakpoint-list
  '((ruby-mode . "require 'pry'; binding.pry")
    (python-mode . "breakpoint()")
    (js-mode . "debugger;"))
  "List of breakpoint commands used by `ptd-insert-breakpoint'.")

(defun ptd-insert-breakpoint ()
  "Insert breakpoint appropriate for mode at point.

Reads breakpoint commands from `ptd-insert-breakpoint-list'."
  (interactive)
  (let ((cmd (assoc major-mode ptd-insert-breakpoint-list)))
    (if cmd
        (insert (cdr cmd))
      (message "No breakpoint found for %s" major-mode))))

(defun ptd-generate-tags (select)
  "Generate a TAGS file in project root.

If SELECT is non-nil, prompt for directory to generate tags for."
  (interactive "P")
  (ptd-project-in-project-root
   select
   (start-process
    "etags"
    "*etags*"
    "ctags"
    "-eR"
    "-f"
    "TAGS"
    ".")))

(defvar ptd-test-file-list
  '((python-mode . (lambda (filename) (format "pytest %s" filename)))
    (java-mode . (lambda (filename) (format "mvn -Dtest=%s test" (file-name-sans-extension (file-name-nondirectory filename))))))
  "List of test commands used by `ptd-test-file'.")

(require 'compile)
(defun ptd-test-file (select)
  "Run test command on current file.

If SELECT is non-nil, prompt for directory to run test command in.

Reads test commands from `ptd-test-file-list'."
  (interactive "P")
  (let ((filename (buffer-file-name))
        (func (assoc major-mode ptd-test-file-list)))
    (if (and filename func)
        (ptd-project-in-project-root
         select
         (compile (apply (cdr func) (list filename)) t))
      (if (not filename)
          (error "No file for current buffer")
        (error "No test command given for current mode")))))

(require 'seq)
(defun ptd-visible-buffers ()
  "Return a list of all visible buffers.

Note: This is not elscreen aware, so it only returns buffers
visible on current screen."
  (reverse (seq-filter 'get-buffer-window (buffer-list))))

(defun ptd-invisible-buffers ()
  "Return a list of all buffers not visible.

Note: This is not elscreen aware, so it only returns buffers
not visible on current screen."
  (seq-filter (lambda (buf) (not (get-buffer-window buf))) (buffer-list)))

(defun ptd-kill-invisible-buffers ()
  "Kill all not currently visible buffers.

Not super recommended, but sometimes things are out of control."
  (interactive)
  (dolist (buf (ptd-invisible-buffers))
    (kill-buffer buf)))

(defun ptd-select-visible-buffer ()
  "Select a visible buffer by name."
  (interactive)
  (let* ((buffer-names (mapcar 'buffer-name (ptd-visible-buffers)))
         (buffer-name (ido-completing-read "Buffer: " buffer-names)))
    (select-window (get-buffer-window buffer-name))))

(defun ptd-copy-filename ()
  "Copies filename for current buffer to kill ring if a filename exists."
  (interactive)
  (let ((filename (buffer-file-name)))
    (if filename
	(kill-new filename))))

(defun ptd-copy-git-sha (select)
  "Copy current git sha to kill ring.

If SELECT is non-nil prompt for directory to run command in."
  (interactive "P")
  (ptd-project-in-project-root
   select
   (kill-new (string-trim (shell-command-to-string "git rev-parse HEAD")))))

(defun ptd-byte-compile-custom-files ()
  "Byte compile all files in the ~/emacs.d/custom directory."
  (interactive)
  (mapcar 'byte-compile-file (file-expand-wildcards "~/.emacs.d/custom/*.el")))

(defun ptd-sql-session ()
  "Start sql buffer and sqli session for given connection.
Loads connections from sql-connection-alist, if present.
Otherwise, does nothing."
  (interactive)
  (let ((available-sql-connections nil))
    (if (boundp 'sql-connection-alist)
	(dolist (conn sql-connection-alist available-sql-connections)
	  (push (symbol-name (car conn)) available-sql-connections)))
    (if available-sql-connections
	(let ((selected-connection (ido-completing-read "Open connection: " available-sql-connections)))
	  (switch-to-buffer (concat selected-connection ".sql"))
	  (sql-mode)
	  (sql-connect (make-symbol selected-connection))
	  (select-window
	   (get-buffer-window (get-buffer (concat selected-connection ".sql")))))
      (message "No connections available. Set them in sql-connection-alist."))))

(defun ptd-python-docs ()
  "Open python module index in an info buffer."
  (interactive)
  (info "(python)Python Module Index"))

(defun ptd-sqlfluff-docs ()
  "Open sqluff top-level index in an info buffer."
  (interactive)
  (info "(sqlfluff)Index"))

(defun ptd-project-pdb (command-line)
  "Run pdb in ptd-project root.

COMMAND-LINE is the pdb command to run."
  (interactive (list (gud-query-cmdline 'pdb)))
  (ptd-project-in-project-root nil (pdb command-line)))

(defun ptd-render-as-html (filename)
  "Renders FILENAME as html and displays in a temp buffer."
  (interactive (list (buffer-file-name)))
  (let ((buffername "*html*"))
    (with-output-to-temp-buffer buffername
      (pop-to-buffer buffername)
      (shell-command
       (concat
        "pandoc -s "
        filename
        " 2>/dev/null")
       buffername)
      (shr-render-region (point-min) (point-max)))))

(defun ptd-jump-to-next-whitespace ()
  "Jumps cursor to next whitespace.

This is useful because the default behavior of
\\[isearch-forward] <space> it to jump to the end of a block on
contiguous whitespace. When editing a copy of sql output or
similar it's frequently useful to instead jump to the beginning
of that whitespace instead."
  (interactive)
  (skip-syntax-forward "^\s"))

(defun ptd-eww-python-docs (module)
  "Open eww with python docs for MODULE."
  (interactive "sModule: ")
  (eww (format "https://docs.python.org/3/library/%s.html" module)))

(defun ptd-xml-indent-set-size (indent-size)
  "Set local buffer xml indent size.

INDENT-SIZE should be an integer number of spaces to indent by."
  (interactive (list (read-number "Indent size: " 4)))
  (set (make-local-variable 'nxml-child-indent) indent-size))

(provide 'ptd-functions)
;;; ptd-functions.el ends here
