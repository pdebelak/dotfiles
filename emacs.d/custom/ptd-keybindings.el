;;; ptd-keybindings.el --- Custom keybindings

;;; Commentary:

;; Some keybindings for commonly used functions and commands

;;; Code:

(require 'ptd-functions)
(require 'ptd-packages)
(require 'ptd-project)

(global-set-key (kbd "C-x C-m") 'smex)
(global-set-key (kbd "M-x") 'smex)

(defvar ptd-user-binding mode-specific-map
  "An alias for \\[mode-specific-map].")

(define-key ptd-user-binding "b" 'ptd-select-visible-buffer)
(define-key ptd-user-binding "g" 'magit-status)
(define-key ptd-user-binding (kbd "C-c M-x") 'execute-extended-command)

;; ----------------------------------------------------------------------------
;; Press "p" for project bindings
;; ----------------------------------------------------------------------------
(define-prefix-command 'ptd-project-binding)
(define-key ptd-user-binding "p" 'ptd-project-binding)

(define-key ptd-project-binding "f" 'ptd-project-find-file)
(define-key ptd-project-binding "g" 'ptd-project-grep)
(define-key ptd-project-binding "t" 'ptd-generate-tags)
(define-key ptd-project-binding "c" 'ptd-project-compile)

;; ----------------------------------------------------------------------------
;; Press "i" for insert bindings
;; ----------------------------------------------------------------------------
(define-prefix-command 'ptd-insert-binding)
(define-key ptd-user-binding "i" 'ptd-insert-binding)

(define-key ptd-insert-binding "b" 'ptd-insert-breakpoint)

;; ----------------------------------------------------------------------------
;; Press "f" for bindings related to current file
;; ----------------------------------------------------------------------------
(define-prefix-command 'ptd-file-binding)
(define-key ptd-user-binding "f" 'ptd-file-binding)

(define-key ptd-file-binding "t" 'ptd-test-file)
(define-key ptd-file-binding "k" 'ptd-copy-filename)

;; ----------------------------------------------------------------------------
;; Press "e" for editing bindings
;; ----------------------------------------------------------------------------
(define-prefix-command 'ptd-editing-binding)
(define-key ptd-user-binding "e" 'ptd-editing-binding)

(define-key ptd-editing-binding " " 'ptd-jump-to-next-whitespace) ;; acts like vim cf<char>
(define-key ptd-editing-binding "t" 'zap-up-to-char) ;; acts like vim ct<char>
(define-key ptd-editing-binding "f" 'zap-to-char) ;; acts like vim cf<char>
(global-set-key (kbd "M-z") 'zap-up-to-char)  ;; more useful than zap-to-char default

;; ----------------------------------------------------------------------------
;; Press "v" for version control bindings
;; ----------------------------------------------------------------------------
(define-prefix-command 'ptd-vcs-binding)
(define-key ptd-user-binding "v" 'ptd-vcs-binding)

(define-key ptd-vcs-binding "b" 'magit-blame-addition)

;; ----------------------------------------------------------------------------
;; Press "d" for documentation bindings
;; ----------------------------------------------------------------------------
(define-prefix-command 'ptd-docs-binding)
(define-key ptd-user-binding "d" 'ptd-docs-binding)

(define-key ptd-docs-binding "p" 'ptd-python-docs)

;; ----------------------------------------------------------------------------
;; Press "l" for lsp bindings
;; ----------------------------------------------------------------------------
(define-prefix-command 'ptd-lsp-binding)
(define-key ptd-user-binding "l" 'ptd-lsp-binding)

(define-key ptd-lsp-binding "c" 'lsp)
(define-key ptd-lsp-binding "d" 'lsp-find-definition)
(define-key ptd-lsp-binding "r" 'lsp-find-references)

;; Press "l s" for lsq-sql bindings
(define-key ptd-lsp-binding (kbd "s c") 'lsp-sql-switch-connection)
(define-key ptd-lsp-binding (kbd "s d") 'lsp-sql-switch-database)
(define-key ptd-lsp-binding (kbd "s s") 'lsp-sql-show-schemas)
(define-key ptd-lsp-binding (kbd "s e") 'lsp-sql-execute-query)

;; ----------------------------------------------------------------------------
;; Press "w" for web bindings
;; ----------------------------------------------------------------------------
(define-prefix-command 'ptd-web-binding)
(define-key ptd-user-binding "w" 'ptd-web-binding)

(define-key ptd-web-binding "p" 'ptd-eww-python-docs)

(provide 'ptd-keybindings)
;;; ptd-keybindings.el ends here
