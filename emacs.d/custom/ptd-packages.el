;;; ptd-packages.el --- Packages from elpa and such

;;; Commentary:

;; Handles installing packages and basic configuration

;;; Code:

;; the package manager
(require 'package)
(setq
 package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                    ("org" . "http://orgmode.org/elpa/")
                    ("melpa" . "http://melpa.org/packages/")
                    ("melpa-stable" . "http://stable.melpa.org/packages/"))
 package-archive-priorities '(("melpa" . 1)))

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;; set path from bash
(use-package exec-path-from-shell
  :ensure t
  :if window-system
  :config
  (exec-path-from-shell-initialize))
;; autocomplete
(use-package company
  :ensure t
  :init
  (global-company-mode))
;; syntax checking
(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode)
  ;; check python with mypy
  (flycheck-define-checker
      python-mypy ""
      :command ("mypy"
                "--ignore-missing-imports"
                source-original)
      :error-patterns
      ((error line-start (file-name) ":" line ": error:" (message) line-end))
      :modes python-mode)

  (add-to-list 'flycheck-checkers 'python-mypy t)
  (flycheck-add-next-checker 'python-flake8 'python-mypy t))
;; better git integration
(use-package magit
  :ensure t
  :commands (magit-status magit-blame-addition))
;; better M-x
(use-package smex
  :ensure t
  :init
  (smex-initialize))
;; ido even more everywhere
(use-package ido-completing-read+
  :ensure t
  :init
  (ido-ubiquitous-mode 1))
;; string conversion helpers
(use-package string-inflection
  :ensure t
  :commands
  (string-inflection-underscore string-inflection-camelcase string-inflection-kebab-case))
;; python stuff
(use-package pyvenv
  :commands pyvenv-activate
  :ensure t)
(use-package eglot
  :hook ((python-mode java-mode go-mode) . eglot-ensure)
  :ensure t)
(use-package python-black
  :ensure t
  :after python
  :init
  (add-hook 'python-mode-hook #'python-black-on-save-mode))
(setq py-isort-options '("--multi-line=3" "--trailing-comma" "--force-grid-wrap=0" "--use-parentheses" "--line-width=88"))
(use-package py-isort
  :ensure t
  :after python
  :config
  (add-hook 'before-save-hook 'py-isort-before-save))
;; go stuff
(use-package go-mode
  :ensure t
  :mode "\\.go\\'"
  :config
  (add-hook 'before-save-hook 'gofmt-before-save)
  (let ((govet (flycheck-checker-get 'go-vet 'command)))
    (when (equal (cadr govet) "tool")
      (setf (cdr govet) (cddr govet)))))
(use-package company-go
  :ensure t
  :after go-mode
  :config
  (add-to-list 'company-backends 'company-go))
;; various modes
(use-package yaml-mode
  :commands yaml-mode
  :mode "\\.yaml\\'"
  :mode "\\.yml\\'"
  :ensure t)
(use-package dockerfile-mode
  :commands dockerfile-mode
  :mode "Dockerfile\\'"
  :ensure t)
(use-package markdown-mode
  :mode "*.md\'"
  :ensure t)
(use-package terraform-mode
  :ensure t
  :mode "\\.tf\\'"
  :init
  (add-hook 'terraform-mode-hook #'terraform-format-on-save-mode))
(use-package php-mode
  :mode "\\.php\\'"
  :ensure t)
(use-package groovy-mode
  :mode "Jenkinsfile\\'"
  :ensure t)
(use-package ess
  :ensure t
  :mode "\\.r\\'")
(use-package yasnippet
  :defer t
  :ensure t)
(use-package sqlformat
  :ensure t
  :after sql)
(use-package protobuf-mode
  :ensure t
  :mode "\\.proto\\'")
(provide 'ptd-packages)
;;; ptd-packages.el ends here
