;;; init.el --- customization entry point

;;; Commentary:

;; Some basic customization

;;; Code:

;; y or n instead of yes or no
(defalias 'yes-or-no-p 'y-or-n-p)

;; Turn off ui
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(unless (display-graphic-p)
  (menu-bar-mode -1))

;; Some good defaults
(setq
 inhibit-startup-screen t
 create-lockfiles nil
 auto-save-default nil
 backup-inhibited t
 make-backup-files nil
 column-number-mode t
 scroll-error-top-bottom t
 sentence-end-double-space nil
 read-process-output-max (* 1024 1024))
(setq-default indent-tabs-mode nil)
(set-frame-parameter nil 'fullscreen 'maximized)

;; Use clipboard normally on linux
(unless (eq system-type 'darwin)
  (progn
    (setq select-enable-clipboard t)
    (setq interprogram-paste-function 'x-cut-buffer-or-selection-value)))

;; save minibuffer history
(savehist-mode 1)

;; Better font size
(set-face-attribute 'default nil :height 140)

;; delete trailing whitespace on save
(add-hook 'before-save-hook
          'delete-trailing-whitespace)

;; remove unwanted bindings
(add-hook 'python-mode-hook
          (lambda()
            (local-unset-key (kbd "C-c C-p"))))
(add-hook 'js-mode-hook
          (lambda()
            (define-key js-mode-map (kbd "M-.") 'xref-find-definitions)))

;; display line numbers in programming mode buffers
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; let emacs find ditaa, if installed
(let ((jar-path
       (if (executable-find "brew")
           ;; brew sets ditaa to a shell script, look in the cellar
           ;; for the jar
           (car
            (file-expand-wildcards
             (concat
              (string-trim (shell-command-to-string "brew --prefix"))
              "/Cellar/ditaa/*/libexec/ditaa*.jar")))
         ;; otherwise, assume that the ditaa in the path is the jar
         (executable-find "ditaa"))))
  (if jar-path
      (setq org-ditaa-jar-path jar-path)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Info-additional-directory-list '("~/.local/share/info"))
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#c5c8c6" "#cc6666" "#b5bd68" "#f0c674" "#81a2be" "#b294bb" "#8abeb7" "#373b41"))
 '(css-indent-offset 2)
 '(custom-safe-themes
   '("628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" "bb08c73af94ee74453c90422485b29e5643b73b05e8de029a6909af6a3fb3f58" default))
 '(fci-rule-color "#373b41")
 '(gc-cons-threshold 100000000)
 '(gofmt-command "goimports")
 '(js-indent-level 2)
 '(ns-alternate-modifier 'super)
 '(ns-command-modifier 'meta)
 '(nxml-child-indent 4)
 '(org-agenda-files nil)
 '(org-babel-load-languages
   '((emacs-lisp . t)
     (ruby . t)
     (python . t)
     (R . t)
     (shell . t)
     (ditaa . t)
     (sql . t)))
 '(org-babel-python-command "python3")
 '(org-startup-with-inline-images t)
 '(package-selected-packages
   '(protobuf-mode eglot ido-completing-read+ string-inflection sqlformat groovy-mode ess yasnippet py-isort web-mode php-mode company-go pyvenv python-black go-mode terraform-mode company company-mode markdown-mode dockerfile-mode yaml-mode smex exec-path-from-shell flycheck magit use-package))
 '(ptd-project-files-command "ptd-grep -f")
 '(ptd-project-git-files-command "ptd-grep -f")
 '(ptd-project-git-grep-template "ptd-grep <R>")
 '(ptd-project-grep-template "ptd-grep <R>")
 '(python-shell-interpreter "python3")
 '(ring-bell-function 'ignore)
 '(ruby-insert-encoding-magic-comment nil)
 '(sh-basic-offset 2)
 '(shr-use-fonts nil)
 '(shr-width 80)
 '(sqlformat-command 'sqlfluff)
 '(truncate-lines t)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#cc6666")
     (40 . "#de935f")
     (60 . "#f0c674")
     (80 . "#b5bd68")
     (100 . "#8abeb7")
     (120 . "#81a2be")
     (140 . "#b294bb")
     (160 . "#cc6666")
     (180 . "#de935f")
     (200 . "#f0c674")
     (220 . "#b5bd68")
     (240 . "#8abeb7")
     (260 . "#81a2be")
     (280 . "#b294bb")
     (300 . "#cc6666")
     (320 . "#de935f")
     (340 . "#f0c674")
     (360 . "#b5bd68")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(add-to-list 'load-path (concat user-emacs-directory "custom"))
(load-theme 'adwaita)

;; use ido
(require 'ido)
(ido-mode 1)
(ido-everywhere 1)
(setq ido-enable-flex-matching t)
(setq ido-use-faces t)
(require 'ptd-packages)
(require 'ptd-project)
(require 'ptd-functions)
(require 'ptd-keybindings)

;; load machine-specific file if it exists
(if (file-exists-p "~/.emacs.d/custom/machine-specific.el")
    (require 'machine-specific))
;;; init.el ends here
