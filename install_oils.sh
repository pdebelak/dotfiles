#!/bin/sh
# Installs osh and oil to ~/.local/bin/

set -e

OIL_VERSION="0.22.0"
OIL_DIR="oils-for-unix-${OIL_VERSION}"
TAR_FILE="$OIL_DIR.tar.gz"
PREFIX="$HOME/.local"

if ! command -v wget >/dev/null; then
  echo 'This script uses wget, please install that first' >&2
  exit 1
fi

wget "https://www.oilshell.org/download/$TAR_FILE"
tar -xf "$TAR_FILE" || echo 'tar failed, proceeding anyway'
cd "$OIL_DIR"

if command -v brew >/dev/null; then
  # configure with brew's readline
  if ! ./configure --prefix "$PREFIX" --with-readline --readline "$(brew --prefix)/opt/readline/"; then
    # hack around bug in configure script - see https://github.com/oilshell/oil/issues/1467
    sed 's/echo '"'"'#define HAVE_READLINE 1'"'"'/echo '"'"'#define HAVE_READLINE 1'"'"' \&\& return 0/g' configure | sh /dev/stdin --prefix "$PREFIX" --with-readline --readline "$(brew --prefix)/opt/readline/"
  fi
else
  ./configure --prefix "$PREFIX" --with-readline
fi

_build/oils.sh
./install
mkdir -p "$PREFIX/share/oils"

cd ..
rm "$TAR_FILE"
rm -rf "$OIL_DIR"
