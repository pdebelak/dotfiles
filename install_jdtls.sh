#!/bin/sh

set -e

jdtls_url="https://www.eclipse.org/downloads/download.php?file=/jdtls/milestones/1.33.0/jdt-language-server-1.33.0-202402151717.tar.gz"
jdtls_path="$HOME/.local/share/jdtls"
tar_path="$jdtls_path/jdtls.tar.gz"

mkdir -p "$jdtls_path"
wget -O "$tar_path" "$jdtls_url"

cd "$jdtls_path"
tar -xzvf "$tar_path"
rm "$tar_path"

ln -sf "$jdtls_path/bin/jdtls" "$HOME/.local/bin/jdtls"
