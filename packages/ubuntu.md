# Ubuntu installed packages

This is a list of packages installed via `apt`. It ignores things that
are installed by default like firefox or extras that might be
installed via the software center.

## Base

The following are needed to run the dotfiles `install.sh` script
including the optional oil install and python info docs scripts.

```shell
apt install -y \
  build-essential \
  curl \
  git \
  libreadline-dev \
  python3-venv \
  golang \
  texinfo
```

## Apps

```shell
apt install -y \
  emacs \
  geary \
  gnome-tweaks \
  lollypop
```

## Blog dependencies

The following are required for working on
[peterdebelak.com](https://www.peterdebelak.com).

```shell
apt install -y \
  ruby-bundler \
  ruby-dev \
  zlib1g-dev
```

## Another Flavor dependencies

The following are required for working on [another
flavor](https://anotherflavor.peterdebelak.com).

```shell
apt install -y \
  hugo
```

## Recipes dependencies

The following are required for working on [my recipes
site](https://pdebelak.gitlab.io/recipes/).

```shell
apt install -y \
  pandoc
```

## Oil dev dependencies

The following are required for working on
[oil](https://github.com/oilshell/oil).

```shell
apt install -y \
  cmake \
  gawk \
  libcmark-dev \
  ninja-build \
  python2-dev \
  python3-pip \
  re2c
```

To install pip2, you'll need to do a manual install.

```shell
curl https://bootstrap.pypa.io/pip/2.7/get-pip.py | python2
```

Then, oil expects some python packages to be install.

```shell
pip2 install --user pyyaml typing flake8 pygments
pip3 install --user mypy
```

## Postgresql python development

To use postgresql and interface with it using python:

```
apt install -y \
  postgresql \
  postgresql-contrib \
  python3-psycopg2
```
