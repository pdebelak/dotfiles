# Fedora installed packages

This is a list of packages installed via `dnf`. It ignores things that
are installed by default like firefox or extras that might be
installed via gnome software.

## Base

The following are needed to run the dotfiles `install.sh` script
including the optional oil install and python info docs scripts.

```shell
dnf install -y \
  readline-devel \
  golang
```

## Apps

```shell
dnf install -y \
  emacs \
  geary \
  gnome-tweaks \
  lollypop
```

## Blog dependencies

The following are required for working on
[peterdebelak.com](https://www.peterdebelak.com).

```shell
dnf install -y \
  gcc-c++ \
  ruby-devel
```

## Recipes dependencies

The following are required for working on [my recipes
site](https://pdebelak.gitlab.io/recipes/).

```shell
dnf install -y \
  pandoc
```
