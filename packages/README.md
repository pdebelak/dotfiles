# Installed packages

This contains notes about installed packages, organized by type, one
file per operating system. The goal is to make it easy to install the
same packages on a computer and to remember what has been installed
for easier clean-up later.

- [Fedora](./fedora.md)
- [Ubuntu](./ubuntu.md)
- [MacOS](./macos.md)
