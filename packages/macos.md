# MacOS installed packages

My employer typically provides a mac for work.

These are preliminary notes since I don't currently have a mac to
test.

## Homebrew

First, install a c compiler and other programming tools:

```shell
xcode-select --install
```

Then, install [homebrew](https://brew.sh/):

```shell
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
eval "$(/opt/homebrew/bin/brew shellenv)"
```

## Base

The following are needed to run the dotfiles `install.sh` script
including the optional oil install and python info docs scripts:

```shell
brew install \
  readline \
  wget
```

## Apps

```shell
brew install --cask \
  emacs \
  firefox
```
