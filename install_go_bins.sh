#!/bin/sh

set -e

export GOPATH="$HOME/.local/share/go"
export GO111MODULE=on

for program in github.com/lighttiger2505/sqls golang.org/x/tools/cmd/goimports golang.org/x/tools/gopls; do
  go install "${program}@latest"
  bin="$(basename "$program")"
  ln -sf "$GOPATH/bin/$bin" "$HOME/.local/bin/$bin"
done
