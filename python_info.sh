#!/bin/sh

set -e

python_version="3.10.2" # best to just use the newest

check_bin() {
  bin_name="$1"
  if ! command -v "$@" >/dev/null; then
    echo "This script requires $bin_name, please install that first" >&2
    exit 1
  fi
}

check_bin wget
check_bin install-info

# download and untar docs
wget "https://www.python.org/ftp/python/doc/${python_version}/python-${python_version}-docs-texinfo.tar.bz2"
tar -xf "python-${python_version}-docs-texinfo.tar.bz2"
rm "python-${python_version}-docs-texinfo.tar.bz2"

# build the docs
curdir="$(pwd)"
cd "python-${python_version}-docs-texinfo"
make infodir="$HOME/.local/share/info" install-info

# clean-up
cd "$curdir"
rm -rf "python-${python_version}-docs-texinfo"
