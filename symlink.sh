#!/bin/sh

DIR=$( cd "$( dirname "$0" )" && pwd )

# vim symlinking
mkdir -p "$HOME"/.vim
mkdir -p "$HOME"/.vim/backup
mkdir -p "$HOME"/.vim/undodir
ln -sf "$DIR/vimrc" "$HOME"/.vimrc

# shell symlinking
ln -sf "$DIR/bashrc" "$HOME"/.bashrc
mkdir -p "$HOME/.config/oils"
ln -sf "$DIR/oshrc" "$HOME/.config/oils/oshrc"
ln -sf "$DIR/kshrc" "$HOME"/.kshrc
ln -sf "$DIR/zshrc" "$HOME"/.zshrc
ln -sf "$DIR/profile" "$HOME"/.profile
mkdir -p "$HOME"/.sh
ln -sf "$DIR"/sh/* "$HOME"/.sh/
mkdir -p "$HOME"/.local/bin
ln -sf "$DIR"/scripts/* "$HOME"/.local/bin/

# other .rc symlinking
ln -sf "$DIR/sqliterc" "$HOME"/.sqliterc
ln -sf "$DIR/ctags" "$HOME"/.ctags
ln -sf "$DIR/hgrc" "$HOME"/.hgrc

# git symlinking
ln -sf "$DIR/gitconfig" "$HOME"/.gitconfig
ln -sf "$DIR/gitignore_global" "$HOME"/.gitignore_global

# emacs symlinking
mkdir -p "$HOME"/.emacs.d
ln -sf "$DIR/emacs.d/init.el" "$HOME"/.emacs.d/init.el
mkdir -p "$HOME"/.emacs.d/custom
ln -sf "$DIR"/emacs.d/custom/* "$HOME"/.emacs.d/custom/
