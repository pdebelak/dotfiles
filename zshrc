if command -v fasd 1>/dev/null 2>&1; then
  eval "$(fasd --init auto)"
fi

ASDF="$HOME/.asdf/asdf.sh"
if [ -f "$ASDF" ]; then
  . "$ASDF"
fi

# eval funcs in PS1
setopt PROMPT_SUBST

# non-matching globs eval to a null string instead of the glob itself
setopt NULL_GLOB

for config_file in "$HOME"/.sh/*.sh; do
  . "$config_file"
done

reset_color() {
  echo $'%{\e[00m%}'
}

set_color() {
  color_num="$1"
  echo $'%{\e['"${color_num}"$'m%}'
}

export PS1="$(non_default_shell_name "zsh")$(set_color 32)"'$(if [ "${PWD#$HOME}" != "$PWD" ]; then printf "~%s" "${PWD#$HOME}"; else printf "%s" "$PWD"; fi)'"$(reset_color) $(set_color 33)"'$(parse_git_branch)'"$(reset_color)$(prompt_sigil) "
